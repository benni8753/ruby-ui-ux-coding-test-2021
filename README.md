# Ruby Play Network Coding Test UI/UX - 2021
**A coding test for UI/UX internships at Ruby Play Network over the summer of 2021/22**
## Background
Ruby Play Network is looking for an intern over the summer period to complete some upgrades on our current UI, help with frontend design work and improve our user experience. This task involves designing and implementing a frontend solution which will allow a user to sign up for a tournament and see the current tournament leaderboards. We are looking for a user friendly design with emphasis on usability and tidiness which delivers the functionality expressed below. You are free to implement the requirements as you see fit, but must use the technologies below and meet all the acceptance criteria.

## Task
Currently our system allows for tournaments to be run where players compete to see who can reveal the most points in a set time period, usually a week. The current flow for entering users into these tournaments and viewing who is winning on the leaderboards is not satisfactory, so you must design a new one. A user must be able to enter their email address and nickname to sign up for the tournament and must be able to view the other users who have entered, their nicknames, points totals and date that they entered. A countdown of some kind should also be shown to inform the user how long the tournament is going to be run for. This test is about frontend design over anything else so the visual aspect of your solution will be weighed heavier than the functionality, but shipping a product with missing or broken functionality will count against you.

## Acceptance criteria
* A user can input their email and nickname to be entered into the tournament and will turn up on the leaderboard
* All current users who are in the tournament are shown on the leaderboard in some form, with their points totals, nicknames and signup date (time) clearly visible
* A countdown is shown displaying the time until the tournament ends
* At least 2 (two) css animations or effects used
* The deliverable is intuitive to use and is visually appealing
* A consistent style is used throughout

## Tools and technologies
* [VueJs](https://vuejs.org/)
* [Quasar Framework](https://quasar.dev/)
* [TypeScript](https://www.typescriptlang.org/)
* A CSS Animation Library (See Tips)

## How to get setup
* Fork this repository to your Bitbucket account, and clone it to your local machine
* Install the dependencies with ` npm install ` while in the repositories main folder
* Complete the task
* To run the site locally for testing purposes use the command `npm run dev`, or look in the package.json file for all possible scripts

## What we are looking for
* Creativity
* Consistent Style
* UI designed with the user experience in mind
* Clean and efficient code
* A responsive layout which handles a range of screen sizes
* Use of appropriate framework capabilities
* Visually appealing

## Deliverable
Code should be committed to a repository hosted on [bitbucket.org](https://bitbucket.org)

## Tips
* Use placeholder data
* A tournament normally runs for about a week
* Points totals during a tournament are usually between 10,000 and 100,000
* You are free to use any CSS animation library you like but listed below are some which may help you get started
  * [Animxyx](https://animxyz.com)
  * [Popmotion.io](https://popmotion.io/pose/learn/vue-get-started/)
  * [Allstars](https://the-allstars.com/vue2-animate/)
* Make your deliverable fun :smirk:
* Dont worry about pagination
* Don't over complicate your solution. There is no need to create a login/signup page or any kind of route security, the task is about design and meeting requirements

